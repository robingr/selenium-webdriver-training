package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Page {

    // Elements:
    @FindBy(css = ".page-header")
    private WebElement pageHeading;

    protected final WebDriver driver;

    Page(WebDriver driver) {
        this.driver = driver;
        // This call sets the WebElement fields.
        PageFactory.initElements(driver, this);
    }

    // Actions:
    public String getPageTitle() {
        return driver.getTitle();
    }

    public String getPageHeadingText() {
        return pageHeading.getText();
    }
}
