package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends Page {

    // Elements:
    @FindBy(css = ".user-info a")
    private WebElement loginLink;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    // Actions:
    public void clickLoginLink() {
        loginLink.click();
    }

}
