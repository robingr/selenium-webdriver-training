package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends Page {

    // Elements:
    @FindBy(name = "email")
    private WebElement emailField;

    @FindBy(name = "password")
    private WebElement passwordField;

    @FindBy(css = "#submit-login")
    private WebElement submitButton;

    @FindBy(css = ".alert")
    private WebElement alertBox;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    // Actions:
    public void enterEmailAddress(String emailAddress) {
        emailField.sendKeys(emailAddress);
    }

    public void enterPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void clickSignIn() {
        submitButton.click();
    }

    public String getErrorMessageText() {
        return alertBox.getText();
    }
}
